<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);
use Slim\Http\Response;

$app->add(new Tuupola\Middleware\JwtAuthentication([
	"path" => "/api",
	"secure" => false,
	"secret" => env("JWT_SECRET"),
	"algorithm" => ["HS256"],
	"ignore" => [
		"/api/token",
		"/api/refresh-token",
	],
	"error" => function (Response $response, $error) {
		return $response->withHeader('Content-Type', 'application/json')
		                ->withJson($error, 401);
	}
]));

$app->add(new \Tuupola\Middleware\HttpBasicAuthentication([
	"path" => "/api/token",
	"secure" => false,
	"authenticator" => new App\Services\BasicAuthenticator($app->getContainer()),
	"error" => function (Response $response, $error) {
		return $response->withHeader('Content-Type', 'application/json')
		                ->withJson($error, 401);
	}
]));