<?php

namespace App\Services;

use App\Models\User;
use Tuupola\Middleware\HttpBasicAuthentication\AuthenticatorInterface;

class BasicAuthenticator implements AuthenticatorInterface {

	/**
	 * AuthenticationService constructor.
	 */
	public function __construct(\Psr\Container\ContainerInterface $container) {
		$this->container = $container;
		$this->db = $container->get('db');
	}


	public function __invoke( array $arguments ): bool {
		$username = $arguments['user'];
		$password = $arguments['password'];
		$user = User::where('username', $username)->first();
		return password_verify($password, $user->password);
	}
}