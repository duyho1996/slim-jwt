<?php

namespace App\Services;


use App\Models\User;
use DateTime;
use Firebase\JWT\JWT;
use Tuupola\Base62;

class AuthService {
	public static function getTokenData( $username ) {
		$now                 = new DateTime();
		$tokenExpired        = new DateTime( "+1 minutes" );
		$refreshTokenExpired = new DateTime( "+1 month" );
		$jti                 = ( new Base62 )->encode( random_bytes( 16 ) );
		$payload             = [
			"iat" => $now->getTimeStamp(),
			"exp" => $tokenExpired->getTimeStamp(),
			"jti" => $jti,
			"sub" => $username
		];
		$secret              = env( 'JWT_SECRET' );
		$token               = JWT::encode( $payload, $secret, "HS256" );


		$refreshTokenJti     = ( new Base62 )->encode( random_bytes( 16 ) );
		$refreshTokenPayload = [
			"iat" => $now->getTimeStamp(),
			"exp" => $refreshTokenExpired->getTimeStamp(),
			"jti" => $refreshTokenJti,
			"sub" => $username
		];
		$refreshToken        = JWT::encode( $refreshTokenPayload, $secret . 'refresh', "HS256" );

		$user                = User::getByUsername( $username );
		$user->refresh_token = $refreshToken;
		$user->save();

		$data["token"]         = $token;
		$data["refresh_token"] = $refreshToken;
		$data["expires"]       = $tokenExpired->getTimeStamp();

		return $data;
	}

	public static function refreshToken( $refreshToken ) {
		$secret  = env( 'JWT_SECRET' );
		$payload = JWT::decode( $refreshToken, $secret . 'refresh', [ "HS256" ] );

		$username = $payload->sub;
		$user     = User::getByUsername( $username );
		if ( $user->refresh_token != $refreshToken ) {
			return [];
		}
		$tokenData = self::getTokenData( $username );

		return $tokenData;
	}
}