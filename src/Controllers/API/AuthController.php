<?php

namespace App\Controllers\API;


use App\Controllers\BaseController;
use App\Models\User;
use App\Services\AuthService;
use DateTime;
use Exception;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Slim\Http\Request;
use Slim\Http\Response;
use Tuupola\Base62;

class AuthController extends BaseController {
	public function getToken( Request $request, Response $response ) {
		/* Here generate and return JWT to the client. */
		//$valid_scopes = ["read", "write", "delete"]
//		$requested_scopes = $request->getParsedBody() ?: [];

		$server   = $request->getServerParams();
		$username = $server['PHP_AUTH_USER'];

		$tokenData = AuthService::getTokenData( $username );

		return $response->withHeader( "Content-Type", "application/json" )
		                ->withJson( $tokenData, 201 );
	}

	public function refreshToken( Request $request, Response $response ) {
		$refreshToken = $request->getParam( 'refresh-token' );

		$tokenData = AuthService::refreshToken( $refreshToken );

		if ( empty( $tokenData ) ) {
			return $response->withHeader( "Content-Type", "application/json" )
			                ->withJson( [ 'message' => 'Invalid refresh token' ], 401 );
		}

		return $response->withHeader( "Content-Type", "application/json" )
		                ->withJson( $tokenData, 201 );
	}
}