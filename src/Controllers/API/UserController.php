<?php
/**
 * Created by IntelliJ IDEA.
 * User: hoduo
 * Date: 8/26/2018
 * Time: 19:30
 */

namespace App\Controllers\API;

use App\Controllers\BaseController;
use App\Models\User;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class UserController extends BaseController {
	public function index( Request $request, Response $response ) {
		$users = User::paginate();

		return $response->withStatus( 200 )
		                ->withHeader( 'Content-Type', 'application/json' )
		                ->withJson( $users );
	}
}