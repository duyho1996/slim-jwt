<?php
/**
 * Created by IntelliJ IDEA.
 * User: hoduo
 * Date: 8/26/2018
 * Time: 22:20
 */

namespace App\Controllers;


use Slim\Container;

class BaseController {

	/**
	 * BaseController constructor.
	 */
	public function __construct(Container $container) {
		$this->container = $container;
		$this->db = $container->get('db');
	}
}