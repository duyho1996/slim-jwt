<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

$container['db'] = function ($container) {
	$capsule = new \Illuminate\Database\Capsule\Manager;
	$capsule->addConnection($container['settings']['db']);

	$capsule->setAsGlobal();
	$capsule->bootEloquent();

	// set currentPage number if it exists
	Illuminate\Pagination\Paginator::currentPageResolver(function ($pageName = 'page') use ($container) {
		$page = $container->request->getParam($pageName);
		if (filter_var($page, FILTER_VALIDATE_INT) !== false && (int) $page >= 1) {
			return $page;
		}
		return 1;
	});

	return $capsule;
};