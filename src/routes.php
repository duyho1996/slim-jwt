<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

/*$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});*/

$app->group('/api', function() {
	$this->post('/token', \App\Controllers\API\AuthController::class . ':getToken');
	$this->get('/refresh-token', \App\Controllers\API\AuthController::class . ':refreshToken');

	$this->group('/users', function() {
		$this->get('', \App\Controllers\API\UserController::class . ':index');
	});
});

