<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class User extends Model{
	protected $table = 'users';
	protected $perPage = 2;

	public $timestamps = false;

	public static function getByUsername($username): User {
		return User::where('username', $username)->first();
	}
}